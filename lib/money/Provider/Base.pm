package money::Provider::Base;

use strict;
use warnings 'all';
use Contenido::Globals;
use payments::Keeper;


sub new {
    my ($proto, %params) = @_;
    my $class = ref($proto) || $proto;
    my $self = {};
    my $prefix = $class =~ /\:\:(\w+)$/ ? lc($1) : undef;
    return	unless $prefix;

    $self->{provider} =		$prefix;
    $self->{app_id} =		$state->{money}->{$prefix."_app_id"};
    $self->{secret} =		$state->{money}->{$prefix."_app_secret"};
    $self->{currency} =		$state->{money}->{$prefix."_currency_code"};
    $self->{test_mode} =	$state->{money}->{$prefix."_test_mode"};

    bless $self, $class;

    return $self;
}


sub id {
    my $self = shift;
    return $self->{app_id};
}

sub app_id {
    my $self = shift;
    return $self->{app_id};
}

sub secret {
    my $self = shift;
    return $self->{secret};
}

sub test_mode {
    my $self = shift;
    return $self->{test_mode};
}

sub currency {
    my $self = shift;
    return $self->{currency};
}

sub currency_code {
    my $self = shift;
    return $self->{currency};
}

sub provider {
    my $self = shift;
    return $self->{provider};
}

#################################
# Пытается зарегистрировать движение средств по order_id.
# В случае успеха возвращает объект money::Movement
# В случае неуспеха выставляет ошибку и возвращает undef.
# Сумма чека в копейках
##########################################################
sub money_movement_register {
    my $self = shift;
    my $opts = shift // {};
    unless ( $opts->{order_id} && $opts->{uid} && $opts->{sum} && $opts->{name} ) {
	$self->{result}{error} = 'Переданы не все обязательные параметры';
	return undef;
    }
    
    my $mm = $keeper->get_documents(
		class		=> 'money::Movement',
		status		=> $self->{test_mode},
		order_id	=> $opts->{order_id},
		order_by	=> 'ctime',
		return_mode	=> 'array_ref',
	);
    my $new = 0;
    if ( ref $mm eq 'ARRAY' && @$mm ) {
	my $last = $mm->[-1];
	if ( $opts->{name} eq 'payment' && $last->name eq 'payment' ) {
		return $last;
	} elsif ( $opts->{name} eq 'refund' && (grep { $_->name eq 'payment' } @$mm) ) {
		$new = 1;
	}
    } elsif ( $opts->{name} eq 'payment' ) {
	$new = 1;
    }
    if ( $new ) {
	$mm = money::Movement->new( $keeper );
	$mm->status( $self->{test_mode} );
	$mm->name( $opts->{name} );
	$mm->order_id( $opts->{order_id} );
	$mm->uid( $opts->{uid} );
	$mm->sum( sprintf("%.2f", $opts->{sum} / 100) );
	$mm->store;
    }
    return $mm;
}

sub get_mm_by_order_id {
    my $self = shift;
    my $order_id = shift;

    my ($mm) = $keeper->get_documents(
			class   => 'money::Movement',
			status  => $self->{test_mode},
			limit   => 1,
			order_id        => $order_id,
			order_by	=> 'ctime desc',
			provider        => $self->{provider},
		);

    return $mm;
}

1;