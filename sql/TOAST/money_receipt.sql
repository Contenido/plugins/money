create sequence money_receipts_id_seq;
select setval('money_receipts_id_seq', 1, true);

create table money_receipts
(
	id integer not null primary key default nextval('public.money_receipts_id_seq'::text),
	class text not null,
	ctime timestamp not null default now(),
	mtime timestamp not null default now(),
	dtime timestamp not null default now(),
	status smallint not null default 0,
	provider text,
	session_id text,
	name text,
	order_id integer not null,
	currency_code varchar(4),
	sum float,
	success smallint default 0,
	data text
);
CREATE INDEX money_receipts_sessions ON money_receipts USING btree (provider, session_id) WHERE session_id is not null;
CREATE INDEX money_receipts_orders ON money_receipts USING btree (order_id);
