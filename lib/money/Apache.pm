package money::Apache;

use strict;
use warnings 'all';

use money::State;
use Contenido::Globals;


sub child_init {
	# встраиваем keeper плагина в keeper проекта
	$keeper->{money} = money::Keeper->new($state->money);
}

sub request_init {
}

sub child_exit {
}

1;
