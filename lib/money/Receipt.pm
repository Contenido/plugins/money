package money::Receipt;

use base "Contenido::Document";
sub extra_properties
{
	return (
		{ 'attr' => 'status',   'type' => 'status',     'rusname' => 'Статус тестирования',
			'cases' => [
					[0, 'Реальный чек'],
					[1, 'Тестовый чек'],
				],
		},
		{ 'attr' => 'comment',	'type' => 'text',	'rusname' => 'Комментарий' }
	)
}

sub class_name
{
	return 'Money: онлайн-чек';
}

sub class_description
{
	return 'Money: онлайн-чек';
}

sub class_table
{
	return 'money::SQL::ReceiptsTable';
}

1;
