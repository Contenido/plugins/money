package money::MovementSection;

use base 'Contenido::Section';

sub extra_properties
{
	return (
		{ 'attr' => 'brief',    'type' => 'text',       'rusname' => 'Описание секции' },
		{ 'attr' => 'default_document_class',		'default' => 'money::Movement' },
		{ 'attr' => '_sorted',				'hidden' => 1 },
		{ 'attr' => 'order_by',				'hidden' => 1 },
	)
}

sub class_name
{
	return 'Money: Секция чеков';
}

sub class_description
{
	return 'Money: Секция чеков';
}

1;
