package money::Init;

use strict;
use warnings 'all';

use Contenido::Globals;
use money::Apache;
use money::Keeper;


# загрузка всех необходимых плагину классов
# money::SQL::SomeTable
# money::SomeClass
Contenido::Init::load_classes(qw(
		money::SQL::ReceiptsTable
		money::Receipt

		money::MovementSection

		money::Provider::Base
		money::Provider::Dreamkas
	));

sub init {
	0;
}

1;
